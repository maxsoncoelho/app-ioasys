
### Dependencias ultilizadas no projeto ###

"dependencies": {
    "@react-native-community/async-storage": "^1.12.1",//ultilizada para salvar dados 
    no aplicativo.
    "@react-navigation/bottom-tabs": "^5.11.8",//ultilizada para criar tabs de navegação 
    na parte inferior.
    "@react-navigation/native": "^5.9.3",//ultilizada para navegação entre componentes.
    "@react-navigation/stack": "^5.14.3",//ultilizada para complementar as navegações.
    "axios": "^0.21.1",//ultilizada para conectar a url base da api.
    "react-native-gesture-handler": "^1.10.3",//complemento das navegações.
    "react-native-screens": "^2.18.0",//complemento das navegações.
    "react-native-vector-icons": "^8.1.0",//ultilizada para exibição de icones.
  },
  
  ### Instruções para instalação do aplicativo ###
  
  instalação via terminal ubuntu:
  1 - Estando na pasta do projeto instale as dependencias requeridas usando - npm install .
  2 - execute o servidor local do react native usando - npm start .
  3 - Agora instale o aplicativo usando - react-native run-android .
  
  
