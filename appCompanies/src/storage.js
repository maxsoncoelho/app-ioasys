import AsyncStorage from '@react-native-community/async-storage';

export const saveUser = async (value) => {
    try {
        const jsonValue = JSON.stringify(value)
      await AsyncStorage.setItem('@storage_User', jsonValue)
    } catch (e) {
      console.log(e)
    }
  }

  export const saveUid = async (value) => {
    try {
      await AsyncStorage.setItem('@storage_Uid', value)
    } catch (e) {
      console.log(e)
    }
  }

  export const saveClient = async (value) => {
    try {
      await AsyncStorage.setItem('@storage_Client', value)
    } catch (e) {
      console.log(e)
    }
  }

  export const saveToken = async (value) => {
    try {
      await AsyncStorage.setItem('@storage_Token', value)
    } catch (e) {
      console.log(e)
    }
  }


  //////

  export const getUser = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_User')
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
      console.log(e)
    }
  }

  export const getUid = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_Uid')
      if(value !== null) {
        console.log(value)
      }
    } catch(e) {
      console.log(error)
    }
  }

  export const getClient = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_Client')
      if(value !== null) {
        console.log(value)
      }
    } catch(e) {
        console.log(error)
    }
  }

  export const getToken = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_Token')
      if(value !== null) {
        console.log(value)
      }
    } catch(e) {
        console.log(error)
    }
  }