import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    
    container: {
      flex:1,
      backgroundColor:'#f0f0f0',
      padding:30
    },
    myProfile: {
        fontSize:20,
        marginVertical:30,
        fontWeight:'bold',
        color:'#000',
        textAlign:'center'
    },
    profileName: {
        fontSize:15,
        marginVertical:10,
        fontWeight:'bold',
        color:'#000'
    },
    profileEmail: {
      fontSize:15,
      marginVertical:10,
      fontWeight:'bold',
      color:'#000'
    },
    profileCity: {
      fontSize:15,
      marginVertical:10,
      fontWeight:'bold',
      color:'#000'
    },
    profileCountry: {
        fontSize:15,
        marginVertical:10,
        fontWeight:'bold',
        color:'#000'
      },
    profileBalance: {
    fontSize:15,
    marginVertical:10,
    fontWeight:'bold',
    color:'#298A08'
    },
    buttonClose: {
      width:70,
      height:30,
      backgroundColor:'#B40431',
      borderRadius:20,
      alignItems:'center',
      justifyContent:'center',
      marginVertical:50,
      marginLeft:220
    },
     textClose: {
      fontSize:15,
      fontWeight:'bold',
      color:'#ffffff'
    },
  });



  export default styles;