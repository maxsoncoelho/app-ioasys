import React, { useEffect,useState } from 'react';
import { KeyboardAvoidingView, Platform, View, ScrollView, 
    TextInput, Text, Alert, Image,TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import logo_ioasys from '../../assets/logo_ioasys.png';
import Icon from 'react-native-vector-icons/dist/Feather';
import api from '../../services/api';
import { saveUser, saveUid, saveClient, saveToken } from '../../storage';
import styles from './styles';



const SignIn = () => {

    const navigation = useNavigation();
    const [email,setEmail] = useState();
    const [password,setPassword] = useState();



    const handleSignIn = async () => {

        if (email == '' || password == '') {
            Alert.alert('Insira email/senha')

        } else {
            try {

                const login = {
                    email:email,
                    password: password
                }

                
                await api.post('/users/auth/sign_in', login )
                .then(async(response) => {

                    if (response.status == 200) {

                        await saveUser(response.data)
                        await saveUid(response.headers.uid)
                        await saveClient(response.headers.client)
                        await saveToken(response.headers['access-token'])

                        api.defaults.headers.common['access-token'] = response.headers['access-token']
                        api.defaults.headers.common['client'] = response.headers.client
                        api.defaults.headers.common['uid'] = response.headers.uid
                        
                        navigation.navigate('AppRoutes')
    
                    } else {
                        Alert.alert('Usuário não permitido')
    
                    }
                })
                
            }
            catch (err) {
                console.log(err)
                Alert.alert('Senha ou email incorretos')
            }
        }
    }


    return (
        <>
            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior={Platform.OS === 'ios' ? 'padding' : undefined}
                enabled>
                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.viewLogo}>
                            <Image style={styles.logo} source={logo_ioasys} />
                        </View>
                        <View>
                            <Text style={styles.textSlogan}>
                                Transformação digital feita por pessoas, 
                                para pessoas
                            </Text>
                        </View>
                        
                        <View>

                            <Text style={styles.textLogin}>Login</Text>
                        </View>
                        <View>
                            <TextInput 
                                style={styles.input}
                                placeholder="Digite seu e-mail"
                                keyboardType="email-address"
                                onChangeText={text => setEmail(text)}
                                placeholderTextColor="#6E6E6E"
                                value={email}
                                selectionColor="#6E6E6E"
                            />
                            <TextInput 
                                style={styles.input}
                                placeholder="Digite sua senha"
                                onChangeText={text => setPassword(text)}
                                value={password}
                                secureTextEntry={true}
                                placeholderTextColor="#6E6E6E"
                                returnKeyType="send"
                                selectionColor="#6E6E6E"
                            />
                        </View>

                        <TouchableOpacity style={styles.buttonLogin} onPress={handleSignIn}>
                            <Icon name="key" size={15} color="#f0f0f0" />
                            <Text style={styles.textButtonLogin}>Entrar</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.forgot}>
                            <Text style={styles.textForgot}>Esqueci minha senha</Text>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
            </KeyboardAvoidingView>

            <TouchableOpacity style={styles.createAccount}>
                <Icon name="log-in" size={20} color="#f0f0f0" />
                <Text style={styles.textAccount}>Criar uma conta</Text>
            </TouchableOpacity>
        </>

    );
}

export default SignIn