import { StyleSheet } from 'react-native'
import { color } from 'react-native-reanimated';

 

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
    },
    title:{
        marginVertical: 15,
        fontSize: 20,
        textAlign: "center",
        color: '#fff',
        fontWeight: "bold"
    },
    viewInput:{
        width:190,
        height:50,
        flexDirection:'row',
    },
    buttonSearch:{
        width:40,
        height:40,
        backgroundColor: '#000',
        borderRadius:30,
        alignItems:'center',
        justifyContent:'center',
        marginLeft:7,
        marginVertical:1,
    },
    iconSearch:{
        marginHorizontal: 10,
        textAlign: "center",
        color: '#fff',
    },
    input:{
        height: 40, 
        borderColor: '#fff', 
        borderWidth: 1, 
        borderRadius:20,
        width:250,
        marginVertical:1,
        paddingLeft: 15,
        marginLeft: 15,
        color:'#fff'
    },
    areaList: {
        width: '100%',
        height: '100%',
        backgroundColor: 'transparent',
        borderColor: '#000', 
        borderWidth: 2, 
    },
    viewButton: {
        width: '100%',
        height: 70,
        backgroundColor: 'transparent',
        borderColor: '#000', 
        borderWidth: 1, 
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row'
    },
    textEnterpriseName: {
        marginLeft: 10, 
        color: '#000',
        fontSize:18,
        fontWeight:'bold'
    }
})

export default styles;