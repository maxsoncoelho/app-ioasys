import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, TouchableOpacity, SafeAreaView, 
    TextInput, Alert,ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/dist/Feather';
import api from '../../services/api';
import styles from './styles';
import ModalDetails from '../../components/ModalDetails/index';



const Home = () => {

    const [modalVisibleDetails, setModalVisibleDetails] = useState(false);
    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState('');
    const [data, setData] = useState([]);
    const [itemEnterprise, setItemEnterprise] = useState();



    useEffect(() => {
        const getList = async () => {
            setLoading(true)
            const response = await api.get('/enterprises')
            const responseJson = await response.data.enterprises
            setData(responseJson)
            setLoading(false)
        }
        getList()
    }, [search]);


    const renderItem = ({ item }) => {

        return (
            <TouchableOpacity onPress={() => onItemPress(item)} style={styles.viewButton}>
                <Text style={styles.textEnterpriseName}>{item.enterprise_name}</Text>
                <Icon name="arrow-right" size={18} color="#000" style={{ marginLeft: '5%' }} />
            </TouchableOpacity>
        )
    }


    const onItemPress = (item) => {
        setItemEnterprise(item)
        setModalVisibleDetails(true)
    }

    const searchNameEnterprise = async () => {

        if(search === '') {
            Alert.alert('Digite o nome da empresa!')
            
        } else {
            const response = await api.get(`/enterprises?name=${search}`)
            setData(response.data.enterprises)
        }
        
    }


    return (

        <View>

            {modalVisibleDetails &&
                <ModalDetails
                    itemEnterprise={itemEnterprise}
                    visible={modalVisibleDetails}
                    visibleAction={setModalVisibleDetails}

                />
            }

            <>
                <View style={{backgroundColor:'#000'}}>
                    <Text style={styles.title}>
                        Lista de Empresas
                    </Text>
                    <View style={styles.viewInput}>
                        <TextInput 
                            style={styles.input}
                            placeholder="Digite o nome da empresa"
                            onChangeText={text => setSearch(text)}
                            placeholderTextColor="#6E6E6E"
                            value={search}
                            selectionColor="#fff"
                            returnKeyType="send"
                        />
                        <TouchableOpacity onPress={searchNameEnterprise} style={styles.buttonSearch} >
                            <Icon name="search" size={18} style={styles.iconSearch} />
                        </TouchableOpacity>
                    </View>
                </View>
                {loading &&
                    <ActivityIndicator size="large" color="#000" style={{marginTop:'50%'}} />
                }
                {!loading &&
                    <SafeAreaView style={styles.areaList}>
                        <FlatList
                            data={data}
                            renderItem={renderItem}
                            keyExtractor={item => item.id.toString()}
                        />
                    </SafeAreaView>
                }
            </>


        </View>
    )
}


export default Home