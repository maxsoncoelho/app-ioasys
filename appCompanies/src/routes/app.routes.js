import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from '../pages/Home';
import MyProfile from '../pages/MyProfile/index';
import Icon from 'react-native-vector-icons/dist/Feather';


const Tab = createBottomTabNavigator();

const AppRoutes = () => (
    <Tab.Navigator
        screenOptions={({ route }) => ({

            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Home') {
                    iconName = focused
                        ? 'home'
                        : 'home';
                } else if (route.name === 'Perfil') {
                    iconName = focused ? 'user' : 'user';
                }
                
                return <Icon name={iconName} size={size} color={color} />;
            },
        })}

        tabBarOptions={{
            activeTintColor: '#40FF00',
            inactiveTintColor: '#fff',
            style: { //Adição do style
                backgroundColor: '#000', // Aplicando a cor ao background
            }
        }}
    >
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Perfil" component={MyProfile} />
    </Tab.Navigator>
);



export default AppRoutes;