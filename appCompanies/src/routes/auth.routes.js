import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SignIn from '../pages/SignIn';
import AppRoutes from './app.routes';


const Auth = createStackNavigator();

const AuthRoutes = () => (
    <Auth.Navigator
        screenOptions={{
            headerShown: false,
            cardStyle: { backgroundColor: '#fff' }
        }}
    >
        <Auth.Screen name='SignIn' component={SignIn} />
        <Auth.Screen name='AppRoutes' component={AppRoutes}/>
    </Auth.Navigator>
);


export default AuthRoutes;