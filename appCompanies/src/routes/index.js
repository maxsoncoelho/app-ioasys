import React, { useEffect, useState } from 'react';
import { StatusBar } from 'react-native';
import AppRoutes from './app.routes';
import AuthRoutes from './auth.routes';
import api from '../services/api';
import Preload from '../components/Preload';
import AsyncStorage from '@react-native-community/async-storage';

const Routes = () => {

    const [appRoute, setAppRoute] = useState(false)
    const [loading, setLoading] = useState(false)



    useEffect(() => {
        async function handleUser() {
            setLoading(true)
            const token = await AsyncStorage.getItem('@storage_Token');
            const client = await AsyncStorage.getItem('@storage_Client');
            const uid = await AsyncStorage.getItem('@storage_Uid');
            setLoading(false)
            if (token) {

                api.defaults.headers.common['access-token'] = token
                api.defaults.headers.common['client'] = client
                api.defaults.headers.common['uid'] = uid

                setAppRoute(true)
            }else{

                setAppRoute(false)
            }
        }
        handleUser()
        
    }, [])

    return (
        
        <>
        <StatusBar backgroundColor="#000000" barStyle="light-content" />

        {loading && // alterar dnv aqui, tirar exclamação
            <Preload />
        }

        {appRoute ? // alterar dnv aqui, tirar exclamação
            <AppRoutes /> :
            <AuthRoutes />
        }

        </>
    )
}


export default Routes;