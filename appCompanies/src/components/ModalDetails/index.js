import React, {useState, useEffect} from 'react';
import { View, Text, TouchableOpacity, Modal, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/dist/Feather';
import styles from './styles';
import api from '../../services/api';



const ModalDetails = ({visibleAction, visible, itemEnterprise}) => {

    const [item, setItem] = useState(itemEnterprise);
    

    const close = () => {
        visibleAction(false)
    }

    useEffect(() => {
        const getDetails = async (itemEnterprise) => {
            console.log(itemEnterprise)
            const response = await api.get(`/enterprises/${itemEnterprise.id}`)
            const enterprise = response.data
            console.log(enterprise)
        }
        getDetails(itemEnterprise)
    }, [])

    return (
        
        <Modal
            animationType="slide"
            transparent={true}
            visible={visible}
            onRequestClose={() => visibleAction(false)}

        >
            <ScrollView>
                <View style={styles.viewCompanies}>
                    <View >
                        <Text style={styles.companyProfile}>Perfil da Empresa</Text>
                        <Text style={styles.companyName}>Nome: {item.city}</Text>
                        <Text style={styles.companyCountry}>País: {item.country}</Text>
                        <Text style={styles.companyCity}>Cidade: {item.city}</Text>
                        <Text style={styles.companyDescription}>Descrição: {item.description}</Text>
                        <Text style={styles.companyPrice}>Valor de Mercado: {item.share_price.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})}</Text>
                    </View>
                    <TouchableOpacity style={styles.buttonClose} onPress={close}>
                        <Text style={styles.textClose}>
                            <Icon name="arrow-right" size={18} color="#fff" />
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </Modal>
    )
}



export default ModalDetails;