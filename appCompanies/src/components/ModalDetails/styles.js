import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    
    viewCompanies: {
      flex:1,
      backgroundColor:'#f0f0f0',
      padding:30
    },
    companyProfile: {
      fontSize:20,
      marginVertical:30,
      fontWeight:'bold',
      color:'#000',
      textAlign:'center'
    },
    companyName: {
      fontSize:15,
      marginVertical:10,
      fontWeight:'bold',
      color:'#000'
    },
    companyCountry: {
      fontSize:15,
      marginVertical:10,
      fontWeight:'bold',
      color:'#000'
    },
    companyCity: {
      fontSize:15,
      marginVertical:10,
      fontWeight:'bold',
      color:'#000'
    },
    companyDescription: {
      fontSize:15,
      marginVertical:10,
      fontWeight:'bold',
      color:'#000'
    },
    companyPrice: {
      fontSize:15,
      marginVertical:10,
      fontWeight:'bold',
      color:'#298A08'
    },
    buttonClose: {
      width:70,
      height:30,
      backgroundColor:'#B40431',
      borderRadius:20,
      alignItems:'center',
      justifyContent:'center',
      marginVertical:100,
      marginLeft:220
    },
     textClose: {
      fontSize:15,
      fontWeight:'bold',
      color:'#000'
    },
  });



  export default styles;