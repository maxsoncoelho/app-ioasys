import React, {} from 'react'
import { Text, View, Image, StyleSheet } from 'react-native';
import logo_ioasys from '../../assets/logo_ioasys.png';

const Preload = () => {
    

    return (
        
        <View style={styles.container}>
            <Image style={styles.logo} source={logo_ioasys} />
        </View>
        
    )
}

const styles = StyleSheet.create({
    container:{
        width:'100%',
        height:'100%',
        backgroundColor:'#f0f0f0',
        alignItems:'center',
        justifyContent:'center',
        
    }
})

export default Preload;