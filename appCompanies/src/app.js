import 'react-native-gesture-handler';
import React from 'react';
import { View, StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Routes from './routes';

const App = () => (
    <NavigationContainer>
        <StatusBar backgroundColor="#0B004E" barStyle="light-content" />
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Routes />
        </View>
    </NavigationContainer>
)

export default App;